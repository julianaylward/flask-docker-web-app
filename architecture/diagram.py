
import os
from diagrams import Diagram
from diagrams.aws.compute import EC2, Batch
from diagrams.aws.database import Dynamodb
from diagrams.custom import Custom

os.chdir("architecture")

#Note - use filename to save with name not equal to title
with Diagram ("Flask Web App", filename="flask-app-architecture") as diag:
    db = Dynamodb("Lighthouse Results")
    lighthouse_batch = Batch("Lighthouse Batch Job")
    lighthouse_compute = EC2("EC2 Compute")
    web_app = Custom("Web App", "heroku.png")
    lighthouse_batch >> lighthouse_compute >> db
    db >> web_app

#Output the diagram
diag


#   with Cluster("3rd Parties"):
#       ext1 = [Custom("Ravelin", "../images/ravelin.png"),
#                Custom("SA360", "../images/SA360.png"),
#               Facebook("Facebook"),
#               Custom("Search Console", "../images/google-search-console.jpg")]