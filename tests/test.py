from flask_app import app

import unittest
import os
from dotenv import load_dotenv, find_dotenv
import json
from datetime import date, datetime


#Import creds - we need these for dynamo
dotenv_path = find_dotenv()
creds = load_dotenv(dotenv_path)

#In docker, our dir structure is different, we import (only) flask_app >> app
os.chdir("flask_app")

class MyTestCase(unittest.TestCase):

    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_contact(self):
        response = self.app.get('/contact')
        self.assertEqual(response.status_code, 200)

    def test_lighthouse(self):
        response = self.app.get('/lighthouse')
        self.assertEqual(response.status_code, 200)
    
    #Test callback, check 
    def test_lighthouse_cb(self):
        response = self.app.get("/callback/getStock?data=" + "firstcontentfulpaint" + "&period=" + "time_series" + "&interval=" + "1m")
        data = json.loads(response.data)
        last_date = data["data"][0]['x'][-1] #As a string
        today_str = datetime.today().strftime('%Y-%m-%d')
        self.assertEqual(last_date, today_str)
        os.remove("static/stats.json")

if __name__ == "__main__":
    unittest.main()
    