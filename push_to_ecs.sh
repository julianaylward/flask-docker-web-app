# THis assumes you are already logged into AWS via the CLI, having run AWS Configure
#https://eu-west-2.console.aws.amazon.com/ecr/repositories/private/278348348837/flask-web-app?region=eu-west-2


aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin 278348348837.dkr.ecr.eu-west-2.amazonaws.com

docker build -t flask-web-app .

docker tag flask-web-app:latest 278348348837.dkr.ecr.eu-west-2.amazonaws.com/flask-web-app:latest

docker push 278348348837.dkr.ecr.eu-west-2.amazonaws.com/flask-web-app:latest