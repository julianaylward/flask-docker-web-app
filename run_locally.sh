
docker stop flask-app
docker rm flask-app

docker build -t flask-web-app .

#Get aws creds to pass during launch
AWS_ACCESS_KEY_ID=$(aws --profile default configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile default configure get aws_secret_access_key)

#Pass aws creds to docker container. These are available in Heroku but otherwise not available locally
docker run -d -p 5000:5000 --name flask-app \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    flask-web-app 
