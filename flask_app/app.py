#https://matplotlib.org/devdocs/gallery/user_interfaces/web_application_server_sgskip.html
#Following matplotlibs examaple above:
#https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py
# Start by setting up logging - heroku is happy with logging to stdout
# https://www.delftstack.com/howto/python/python-logging-stdout/

#Post data back from a text box and push to slack/dynamo
#https://stackoverflow.com/questions/12277933/send-data-from-a-textbox-into-fl

#Using asych javascript calls to retrive and render plotly charts
#https://github.com/alanjones2/Flask-Plotly/blob/main/fin/app.py



# TODO: a,b,c,d, e


import logging
import sys

#Heroku captures anything printed to stdout in its logs
#Creating and Configuring Logger
Log_Format = "%(levelname)s %(asctime)s - %(message)s"

logging.basicConfig(stream = sys.stdout, 
                    format = Log_Format, 
                    level = logging.INFO)

logger = logging.getLogger()

# Import other libraries
from datetime import datetime
#timestamp when the job starts running
timestampStr = datetime.now(tz=None).strftime("%Y-%m-%d %H:%M:%S") 
#Log commence
logger.info('Starting script: ' + timestampStr)
import boto3
import base64
import tempfile
from os import environ
from flask import Flask, render_template, request

import numpy as np
import pandas as pd
import requests
import json

#from matplotlib.figure import Figure
#TODO get rid of matplotlib at some point
import matplotlib.pyplot as plt 
import plotly
from plotly import express as px
import plotly.graph_objects as go


app = Flask(__name__)

# Fetch some data, ideally it would be something worth showing!
def get_data(multipler):
    values = [8,5,12,6]
    values2 = []
    for i in values:
        #print(i)
        i = i * multipler
        #print(i)
        values2.append(i)
        #print(values2)
    return values2


def create_bar_chart(values,var):
    labels = ["Cheese","Bread","Eggs","Sausages"]
    vals = values
    x = np.arange(len(labels))
    width = 0.35
    fig, ax = plt.subplots()
    rects = ax.bar(x, vals, width, label="Pack Size",align = "center")
    ax.set_ylabel("units")
    ax.set_xlabel("Item Name")
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    #ax.bar_label(rects, padding=3)
    fig.tight_layout()
    # Save it to a temporary buffer.
    buf = BytesIO()
    #save to buffer
    fig.savefig(buf, format="png")
    #save to file
    filename = f"static/chart_{var}.png"
    fig.savefig(filename)
    logger.info(f"saved chart {filename}")
    # Embed the result in the html output.
    #data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return filename

#Slack webhook - post message
def slack_webhook(message):
    webhook_url = "https://hooks.slack.com/services/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"
    slack_data = {'text': message}
    response = requests.post(webhook_url, data= json.dumps(slack_data), headers={'Content-Type': 'application/json'})
    logger.info(f"message: {message} posted to slack, status: {response.status_code}")


#Put result to dynamo
def put_result(datetime, site, results, dynamodb=None):
    logger.info("about to put to dynamo..")
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb',  region_name='eu-west-2') 
    table = dynamodb.Table('test_flask_app')
    response = table.put_item(
       Item={
            'datetime': datetime,
            'site': site,
            'results': results
        }
    )
    return response


# Use table scan method to return all items from Dynamo table
def dynamo_get_lighthouse():
    logger.info("calling dynamo...")
    dynamodb = boto3.resource('dynamodb', region_name="eu-west-2")
    table = dynamodb.Table('lighthouse_results')
    response = table.scan()
    data = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
        data.extend(response['Items'])
    logger.info("returned dynamo results")
    return data

# Process lighthouse results data from dynamo and return as a pandas df
#Get site, datetime and extract first contentful pain as a POC from the data



def process_lighthouse_data(results, metric):
    results_dict = {}
    for result in results:
        load_date_string = str(result["datetime"])
        load_date = (datetime.strptime(load_date_string,'%Y%m%d%H%M%S')).date()
        results_dict.update({result["site"] + "_" + str(result["datetime"]): 
                            {"site": result["site"],
                            "run_timestamp" : result["datetime"],
                            "load_date" : load_date,
                            metric : int(result["results"][metric]),
                            #"largest_contentful_paint" : int(result["results"]["largestcontentfulpaint"]),
                            "run_timestamp" : result["results"]["run_timestamp"]
                            }
                            })
    df = pd.DataFrame.from_dict(results_dict, orient='index')
    return df

def lighthouse_charts(metric):
    logger.info("creating lighthouse chart...")
    lighthouse_results = dynamo_get_lighthouse()
    results_df = process_lighthouse_data(lighthouse_results, metric)
    #aggregate to time series with mean metric val
    df_ts = pd.pivot_table(results_df, values=[metric], 
                       index=['load_date',],
                       columns=['site'], #split columns by e.g. channel grouping if we wanted to 
                       aggfunc=np.mean)
    df_ts.columns = [x[1] for x in df_ts.columns.ravel()] #Remove multi level df cols
    fig1 = px.line(df_ts, x=df_ts.index, y= [x for x in df_ts.columns], color_discrete_sequence=px.colors.qualitative.Dark24)
    fig1.update_traces(connectgaps=True)
    fig1.update_layout(
                xaxis_title="Date",
                yaxis_title=f"{metric} (ms)",
                font=dict(size=16) #pass additional font params here if required
                )
    #Get mean values by site for all time
    df_avg = results_df.groupby(by=["site"]).mean().astype(int)
    x = df_avg.index.to_list()
    y = df_avg[metric].to_list()
    fig2 = go.Figure(go.Bar(x=x, y=y))
    fig2.update_layout(xaxis={'categoryorder':'total ascending'}, title=f"Average {metric} All Time",
                    xaxis_title=f"{metric} (ms)",
                    yaxis_title="Retailer",
                    font=dict(size=16)
                    )
    #Dump charts to JSON
    graph1JSON = json.dumps(fig1, cls=plotly.utils.PlotlyJSONEncoder)
    graph2JSON = json.dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)
    stats = {"best" : int(df_avg.min().values),
        "worst" : int(df_avg.max().values),
        "mean": int(df_avg.mean().values)
        }
    
    # Save it to a temporary file.

    filename = "static/stats.json"
    #dump to json - we need to retieve later without calling dynamo twice
    with open(filename,"w") as jsonfile:
        json.dump(stats, jsonfile)

    logger.info("Successfully created lighthouse charts")
    return graph1JSON, graph2JSON #, best, worst, mean


#This page is not included in the nav for now
#Route decorator creates a single url equal to the function name
@app.route("/matplotlib-flask")
def matplotlib():
    logger.info("Calling homepage...")
    #Fetch some data
    var = 2
    values = get_data(var)
    #Create and save the chart as a static png:
    filename = create_bar_chart(values,var)
    var = 1
    values = get_data(var)
    filename2 = create_bar_chart(values,var)
    logger.info("successfully created homepage charts")
    return render_template("matplotlib-flask.html",name="Julian",title = "Matplotlib and Flask")    #,data=data)

#A second route decorator holds another url and function
#@app.route("/hello-world")
#def hello2(name=None):
#    logger.info("Calling Hello World...")
#    return render_template("about.html",name="Kasia", title = "Work in progress")

#About page
@app.route("/")
def home(name=None):
    logger.info("Calling Home...")
    return render_template("home.html", title = "Home")


# Hide this from the nav for now, its just a placeholder
@app.route("/web-scraping")
def web_scraping():
    logger.info("calling web scraping page...")
    return render_template("web_scraping_members_of_parliament.html",title = "Web Scraping: Members of Parliament")



@app.route("/contact")
def contact():
    logger.info("Calling /contact")
    return render_template("contact.html", title = "Contact")


#This is posted TO, triggered on form submission
@app.route('/contact', methods=['POST'])
def my_form_post():
    text = request.form['text']
    processed_text = text.upper()
    logger.info(f"reveived form data: {processed_text}...")
    #Post text to dynamodb
    now = datetime.now()
    try:
        put_resp = put_result(int(now.strftime("%Y%m%d%H%M%S")), 
                            "test-flask-app", 
                            { "Processed Text": processed_text} 
                            )
        slack_webhook(f"successfully put {processed_text} to dyanmo")
        logger.info("successfully put form data to dynamo")
    except:
        logger.info("failed to put to dynamo")
        slack_webhook("failed to put to dynamo")
    #Post the put response to slack webhook
    return render_template("contact.html", title = "contact-form") #We can return a different template/variable here post submission


@app.route("/lighthouse")
def lighthouse():
    logger.info("Calling /lighthouse")
    return render_template("lighthouse.html", title = "Google Lighthouse Benchmarking")


@app.route('/callback/<endpoint>')
def cb(endpoint): 
    logger.info("lighthouse callback")  
    if endpoint == "getStock": #TODO change the name here
        metric, chart, time_period = request.args.get('data'), request.args.get('period'), request.args.get('interval')
        graph1, graph2 = lighthouse_charts(metric)
        if chart == "time_series":
            return graph1  #Just 1 variable for now #,request.args.get('period'),request.args.get('interval'))
        else:
            return graph2
    elif endpoint == "getInfo":
        metric, chart = request.args.get('data'), request.args.get('period')
        with open('static/stats.json') as f:
            data = json.load(f)
        if chart == "time_series":
            st = {"shortName": f"{request.args.get('data')} by day in milliseconds", "metric": metric, "best": data["best"], "worst": data["worst"], "mean" : data["mean"]} 
        else:
            st = {"shortName": f"{request.args.get('data')} by site in milliseconds", "metric": metric, "best": data["best"], "worst": data["worst"], "mean" : data["mean"]}
        return json.dumps(st)
    else:
        return "Bad endpoint", 400



if __name__ == '__main__':
    #app.run(debug=True, host='0.0.0.0')
    app.run(host='0.0.0.0', debug=False, port=environ.get("PORT", 5000))

#Close the logging session - is this relevant for an app?
logging.shutdown()

