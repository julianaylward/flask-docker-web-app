FROM python:slim-stretch

#Set the WD
WORKDIR /usr/src/app 

#Copy requirements.txt
COPY requirements.txt ./

RUN pip install --upgrade pip

#Install deps with pip
RUN pip install --no-cache-dir -r requirements.txt

#Copy the CONTENTS of the flask dir to the working dir
COPY ./flask_app/. .

#Configure the flask app
#RUN export FLASK_APP=app
#RUN FLASK_ENV=development

#Run the flask app
#CMD ["python", "-m", "flask", "run"] 

ENTRYPOINT [ "python", "app.py"]

#CMD ["app.py"]
