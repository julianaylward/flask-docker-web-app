# flask-docker-web-app
## Using Flask to create a simple website we'll package up with Docker and run on Fargate.

We can take advantage of docker and serverlesss Containers from AWS fargate to create an efficient website without needing to configure any infrastructure for hosting.

1. Using Flask we'll create a simple app
2. We'll add some html content and style them using CSS
3. Package up with Docker
4. Push to Amazon Elastic Container Registry
5. Run on AWS Fargate


## Flask App
- For demonstration purposes only, our flask app creates a few sample charts using matplotlib
- We are using a base html file to create a header across all pages
- And some styling with CSS which sits in `/static/css/main.css` which we link to the base.html


## Dockerfile
- We can use the slim stretch base python image as we're just need python
- We need to install flask and matplotlib as dependencies in requiements.txt


## Docker Container Directory structure
- In the Dockerfile we copy everything into `/usr/src/app` so the Docker container directory structure looks like this:

. <br>
+-- requirements.txt <br>
+-- app.py <br>
|   +-- templates <br>
|   +-- static <br>
|       +-- css <br>


## To build the Docker Container and run locally:
- In the working directory, run `docker build -t flask-web-app .`
- Port mapping: You must map the docker port back to a local port. Flask runs by defualt on port 5000
- Run the container, either in interactive mode (`-it`) or detached (`-d`) `docker run -d -p 5000:5000 --name flask-app flask-web-app`
- To view the app running on the ec2 (locally) - you need to enable inbound access to the EC2 on port 5000 (custom TCP) in order to access the app
- Connect with <Public IPv4 DNS:5000> where 5000 is the port number


## Unit testing locally
- `python -m unittest discover -s tests -t .`

## AWS Elastic Container Registry
- Using the AWS cli (see here for [installation](https://gitlab.com/julianaylward/slack-airflow/-/wikis/Amazon-CLI-and-Amazon-Elastic-Container-Registry))
    - run `aws configure` and log in with an access key and secret access key
- Log into the AWS Console, navigate to Elastic Container Registry and create a new repository using default setings
- Click View Push Commands
- Then following the instructions retrieve an authentication token to authenticate your Docker client to your registry
- Build your container image if you haven't already `docker build -t flask-web-app .`
- Following instructions from the console, tag your image
- Finally push your image, again, using the instructions
- Run `bash push_to_ecs.sh` to complete the above processes
- More info available from this [blog post](https://towardsdatascience.com/deploy-your-python-app-with-aws-fargate-tutorial-7a48535da586)


## Launch on Fargate
- Following this [blog post](https://towardsdatascience.com/deploy-your-python-app-with-aws-fargate-tutorial-7a48535da586)
    - Go to AWS Elastic Container Service in the console
    - Create a new Networking only cluster
    - Under Tasks Definitions in the LHS of the console, create a new task defition to run on your cluster on Fargate
    - Attached the container image, using the image URI (<account_id>.dkr.ecr.eu-west-2.amazonaws.com/<image>:latest)
    - Create the task
    - From the next screen, execute the task you've defined (Actions >> Run Task)
    - When configuring the security group, you need to expose port 5000 (Custom TCP) since this is the port Flask runs on
    - Once running, click on the task, and the ENI ID. Under the network interfaces, copy the Public IPv4 DNS (e.g. ec2-18-133-73-214.eu-west-2.compute.amazonaws.com, add the port number for flast (IPv4DNS:5000) and paste it into the browser and hopefully you'll see your app!


## Create a domain and route traffic by creating a record in Route53
- Using the AWS Console, select a domain name and purchase if necessary (e.g. www.my-website.com)
- Then, once available, [create a record](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-creating.html) in Route 53
- Go to Hosted Zones in the nagivation pane for Route53
- Select your domain and click "create record"
- Use the Wizard option to keep things simple

## Fargate Cluster Service:
- Create a new service for your cluster
- Enable the creation of a namespace
- And configure the network
- Under Service Discovery
    - create a new Route53 namespace
    - And service discovery name
    - Then configure the DNS Records for service discovery
    - In additional to an `A` record type, configure the `SRV` type which we can point to our container impage and port by selecting the container from the dropdown
    - Set Autoscaling (if required)
    - Launch Service!

## Lauch on Heroku (using Heroku Git via the CLI)
*Note that we need to configure the host to 0.0.0.0 in app.run (in app.py) and import the "PORT" env var to import herokus default port*
- For an UBUNTU linux distro (issues installing on Amazon Linux v2)
- Register an account with Heroku
- Install snapd package installer if requried `sudo apt-get insatll snapd`
- Install heroku CLI using `curl https://cli-assets.heroku.com/install.sh | sh` (not sure why snapd doesn't work for me)
- Login via the CLI with `heroku login` (you many need to do this periodically if/when you push commands hang)
- From within your project, add a new remote git branch for your heroku app `heroku git:remote -a julian-flask-web-app`
- You have a couple of choices on how to deploy, You can use the heroko.yml file rather than pushing a pre-built container to the heroku container registery, following this tutorial [here](https://www.youtube.com/watch?v=eN1EG4-V3Yg)
- Like this: `git push heroku master` where heroku is an additional remote we need to set
- Note that heroku will only accept pushes from the master branch, so dont cut corners. Test locally, merge, push

- Alternatively, we can push a container image to the heroku container registry:
    - Set the app stack in heroku to container (from default ubuntu) using `heroku stack:set container`
    - Build the container image if you haven't already (see above)
    - Push the container `heroku container:push web`
    - Release the container `heroku container:release web`
    - The app will be visible on `https://<my-app-name>.herokuapp.com/`


## Test locally with Heroku:
- Following documentation [here](https://devcenter.heroku.com/articles/heroku-local)
- Run `heroku local` which uses the Procfile and local .env file
- Default port is 5000



## Set config vars (env creds) in Heroku:
- We can set config vars in Heroku required for e.g. boto3 to interact with AWS resources
- run `python load_env_creds.py` to load env creds from a .env file using the heroku cli via the subprocess library
- Initially we are loading keys for an AWS IAM role to call lambdas/dynamo


## Attach a domain and configure DNS
- Before adding a domain you need to verify your details by entering a credit card
- Following the doc [here](https://help.heroku.com/MTG1BIA7/how-do-i-connect-a-domain-to-my-heroku-app)
- Purchase a domain, ideally using one of the dns providers heroku recommend (with an ANAME or ALIAS record type)
- After purchasing a domain (julianaylward.co.uk) in this case purchased via namecheap.com, add a domain and subdomain as follows to your app via the heroku cli (or via the browser) `heroku domains:add julianaylward.co.uk`, `heroku domains: add www.julianaylward.co.uk`
- Following this guide [here](https://www.namecheap.com/support/knowledgebase/article.aspx/9737/2208/pointing-a-domain-to-the-heroku-app/), and this tutorial [here](https://www.youtube.com/watch?v=0zb-FRAJFYE), under "advanced DNS" in your domain in namecheap, add or update the existing dns records as follows:
    - Set/create a ALIAS type record host to `@` and the value to the DNS target we can find running `heroku domains` for the "root" domain (example.com)
    - Set/create a CNAME tyoe record host to `www` and value to the DNS target for the subdomain (www.example.com)
    - If the domain has default DNS records, remove these if you created new ones in the steps above to avoid conflicts
- That's it! It can take a few hours while for the changes to take effect so sit tight.

# SSL Certificates
- SSL Certificates require a paid dyno type (e.g. Hobby) with Heroku


## Free dyno usage
- For a web app with verified user you'll receive 1000 free hours
- sleeping apps don't use up hours
- check an apps quota with heroku ps -a my-app-name








