#Load AWS and other env creds to Heroku from a dotenv file
#Use subprocess to execute the heroku CLI via bash from Python
#See option3 regarding how boto3 works with environment variables (config vars in heroku)
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
#https://devcenter.heroku.com/articles/config-vars



import subprocess
from dotenv import find_dotenv, load_dotenv
import os

dotenv_path = find_dotenv()
load_dotenv(dotenv_path)

creds = {"AWS_ACCESS_KEY_ID": os.environ["AWS_ACCESS_KEY_ID"],
    "AWS_SECRET_ACCESS_KEY"  : os.environ["AWS_SECRET_ACCESS_KEY"]
    }

def creds_to_heroku(key,val):
    subprocess.check_call(f"heroku config:set {key}={val}", shell=True)
    print(f"successfully set config var {key}")

if __name__ == "__main__":
    for cred in creds:
        creds_to_heroku(cred, creds[cred])



